# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.21] - 2024-01-15
* Change error logs to info logs.

## [0.0.20] - 2023-06-13
* Minor release with better logging.

## [0.0.19] - 2023-06-13
* Modify "not found" routine to allow [ and { as starting characters in the JSON. Fixes [RD-134](https://crossref.atlassian.net/browse/RD-134). 

## [0.0.18] - 2023-06-13
* Proposed fix for [problems with async fetch](https://crossref.atlassian.net/jira/software/c/projects/RD/boards/31?modal=detail&selectedIssue=RD-117).

## [0.0.17] - 2023-06-11
* Additional logging

## [0.0.16] - 2023-06-11
* Fallback on async fetch.

## [0.0.15] - 2023-06-02
* Add config to secrets manager object

## [0.0.14] - 2023-05-10
* Add ability to retrieve list of files deleted with delete_under_prefix

## [0.0.13] - 2023-05-10
* Remove accidental print statement

## [0.0.12] - 2023-05-10
* Add ability to delete all objects under a prefix

## [0.0.11] - 2023-04-20
* Add ability to list a prefix

## [0.0.10] - 2023-04-19
* Add ability to fetch secrets
* Add ability to specify region in constructor

## [0.0.9] - 2023-04-19
* Add method to push to S3 using streaming approach

## [0.0.8] - 2023-04-18
* Add option to use signed config

## [0.0.7] - 2023-03-31
* Switch to create objects using session 

## [0.0.6] - 2023-03-31
* Add ability to get a CloudWatch logs client

## [0.0.5] - 2023-03-31
* Add ability to conditionally specify bucket name

## [0.0.4] - 2023-03-31
* Allow optional bucket object creation

## [0.0.3] - 2023-03-30
- Further dependency versioning for aioboto compatibility

## [0.0.2] - 2023-03-30
- Freeze of versions in requirements.txt

## [0.0.1] - 2023-03-30

- First version

